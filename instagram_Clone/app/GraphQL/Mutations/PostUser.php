<?php

namespace App\GraphQL\Mutations;

use App\blocked_user;
use App\Comment;
use App\Event\NotificatonFollow;
use App\Event\NotificatonLike;
use App\Event\NotificatonLikedelete;
use App\FollowUser;
use App\Post;
use App\User;
use GraphQL\Type\Definition\ResolveInfo;
use http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Nuwave\Lighthouse\Console\IdeHelperCommand;
use Nuwave\Lighthouse\Exceptions\AuthenticationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Event\NotificatonUser;

class PostUser
{
    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        // TODO implement the resolver
    }

    #user add post
    public function userPostAdd($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
//        event(new NotificatonUser("post is created"));
        $post_type = ['jpg', 'jpeg', 'png', 'gif'];
        $image = $args['file'];
        $extension = $image->getClientOriginalExtension();
        $originalname = $image->getClientOriginalName();
        $path = in_array($extension, $post_type) ? $image->storeAs(Auth::id(), $originalname, 'public') : $image->storeAs('userpost', $originalname, 'public');
        $post = Post::create(['user_id' => $args['user_id'], 'media_url' => $path, 'post_type' => in_array($extension, $post_type) ? 'image' : 'video', 'caption' => $args['caption'],]);
        return $post;
    }

    #userpostcomment
    public function userComment($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {

        $post_id = $args['post_id'];
        $user = User::find(Auth::id());
        $post = Comment::create([
            'user_id' => $user,
            'post_id' => $args['post_id'],
            'comments' => $args['comments'],
        ]);

        event(new NotificatonUser($post));

        return [
            'user'=>$user->refresh(),
            'meta' => ['message' => 'comment add', "success" => true, "code" => 200,]];
    }

    #post like
    public function postLike($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {

        $post = Post::with('userslike')->find($args['post_id']);
        $post->userslike()->toggle(Auth::id());

        if (sizeof($post->userslike) == null) {
            event(new NotificatonLike($post));
            return ['post' => $post, 'meta' => ['message' => 'You are Like', "success" => true, "code" => 200,]];
        } else {
            event(new NotificatonLikedelete($post));
            return ['post' => $post, 'meta' => ['message' => 'You are DisLike', "success" => true, "code" => 200,]];
        }

    }
}
