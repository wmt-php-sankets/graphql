<?php

namespace App\Listener;

use App\Event\NotificatonFollow;
use App\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class follow
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NotificatonFollow $event
     * @return void
     */
    public function handle(NotificatonFollow $event)
    {

        $user_id = Auth::id();
//        $user_name=Auth::user()->name;
        $user = Notification::create([
            'title' => "You are follow",
            'description' => "follow description",
            'user_id' => $user_id,
            'create_user_id' => $event->user->id,
            'notification_type' => "Follow"
        ]);

    }
}
