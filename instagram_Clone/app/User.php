<?php

namespace App;
//namespace Mpociot\Firebase\Tests\Fixtures;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;
use Laravel\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Mpociot\Firebase\SyncsWithFirebase;

class User extends Authenticatable implements JWTSubject
{

    use Notifiable, Searchable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    public function post(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'services');
    }

    public function posts(): HasMany
    {
        return $this->hasMany(Post::class,'user_id');
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function blocks(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'blocked_user', 'user_id', 'blocked_user_id');
    }

    public function blockUnblock(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'blocked_user', 'blocked_user_id', 'user_id')->withTimestamps();;
    }

    public function follows(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'follow_users', 'user_id', 'Followed_user_id');
    }


    public function followedUnfollowUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'follow_users', 'Followed_user_id', 'user_id')->withTimestamps();
    }

    public function social_user(): HasMany
    {
        return $this->hasMany(Social_user::class);
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
        // TODO: Implement getJWTIdentifier() method.
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims()
    {
        return [];
        // TODO: Implement getJWTCustomClaims() method.
    }
//    public function chats() {
//        return Chat::whereInitiatorId($this->id)->orWhere('partner_id', $this->id)->get();
//    }
    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

}
