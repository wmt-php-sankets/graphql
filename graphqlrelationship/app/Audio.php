<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Audio extends Model
{
    public  function comments(): MorphMany
    {
            return $this->morphMany('App\Comment','commentable');
    }
}
