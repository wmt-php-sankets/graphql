<?php

namespace App\Listener;

use App\Event\NotificatonLikedelete;
use App\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class likedelete
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NotificatonLikedelete $event
     * @return void
     */
    public function handle(NotificatonLikedelete $event)
    {

        $user_id = Auth::id();
        $created_user = $event->post_details->user_id;
        $data = Notification::where('user_id', $user_id)
            ->where('create_user_id', $created_user)
            ->delete();
    }
}
