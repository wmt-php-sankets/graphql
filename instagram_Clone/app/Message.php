<?php

namespace App;
use App\Traits\SyncsWithFirebase;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use SyncsWithFirebase;
    protected $fillable=['body','chat_id','sender_id'];

}
