<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Event\NotificatonUser' => [
            'App\Listener\comment'
        ],
        'App\Event\NotificatonLike' => [
//            'App\Listener\PostAdd',
            'App\Listener\like',
//            'App\Listener\follow',
        ],
        'App\Event\NotificatonFollow' => [
            'App\Listener\follow',

        ],
        'App\Event\NotificatonFollowdelete' => [
            'App\Listener\followdelete'
        ],
        'App\Event\NotificatonLikedelete' => [
            'App\Listener\likedelete',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
