<?php

namespace App\Listener;

use App\Event\NotificatonFollow;
use App\Event\NotificatonFollowdelete;
use App\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class followdelete
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NotificatonFollow $event
     * @return void
     */
    public function handle(NotificatonFollowdelete $event)
    {
        $user_id = Auth::id();
        $created_user = $event->user->id;
        $data = Notification::where('user_id', $user_id)
            ->where('create_user_id', $created_user)
            ->delete();
    }
}
