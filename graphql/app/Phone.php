<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table=phones;

    public function Person()
    {
        return $this->belongsTo('App\Person');
    }

}
