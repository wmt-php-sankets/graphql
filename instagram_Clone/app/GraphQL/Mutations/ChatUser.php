<?php

namespace App\GraphQL\Mutations;
use Illuminate\Database\Eloquent\Collection;
use App\Chat;
use App\Message;
use App\Comment;
use App\User;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Auth;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Mpociot\Firebase\SyncsWithFirebasease;

class ChatUser
{

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        // TODO implement the resolver
    }

//    userCreateChat
    public function userCreateChat($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $chat = Chat::exists($args['Patner_ID'], Auth::id()) ? Chat::exists($args['Patner_ID'], Auth::id()) : Chat::create(['initiator_id' => Auth::id(), 'partner_id' => $args['Patner_ID']]);
            if ($chat){
                return ['chat' => $chat, 'meta' => ['message' => 'Chat created Successfully', "success" => true, "code" => 200,]];
    } else{
                return ['chat' => $chat, 'meta' => ['message' => 'Chat is not created', "success" => false, "code" => 401,]];
            }
    }

    public function sendMessage($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $chat = Chat::find($args['partner_id']);
        if ($chat->initiator_id !== Auth::id() && $chat->partner_id !== Auth::id()) {
            return ['meta' => ['message' => 'Message Does Not Send', "success" =>false, "code" => 401]];
        }else {
            $message = Message::create(['body' => $args['message'], 'sender_id' => Auth::id(), 'chat_id' => $chat->id,]);
            return ['message' => $message, 'meta' => ['message' => 'Message send Successfully', "success" => true, "code" => 200,]];
        }
    }
    public function chat($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $chat=Chat::where('initiator_id',Auth::id())->orWhere('partner_id',Auth::id())->get();
        return $chat;
    }

    public function message($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $message = Message::where('chat_id',$args['chat_id'])->get();
        return $message;
    }

}
