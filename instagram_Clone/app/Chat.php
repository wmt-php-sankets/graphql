<?php

namespace App;
use App\Traits\SyncsWithFirebase;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Chat extends Model
{
    use SyncsWithFirebase;

    protected $fillable = ['initiator_id', 'partner_id'];

    public function initiator() : BelongsTo
    {
        return $this->belongsTo(User::class, 'initiator_id');
    }

    public function partner() : BelongsTo
    {
        return $this->belongsTo(User::class, 'partner_id');
    }

    public static function exists($user1, $user2)
    {
        return Chat::where(function($q) use ($user1, $user2) {
            $q->where('initiator_id', $user1)->where('partner_id', $user2);
        })->orWhere(function($q) use ($user1, $user2) {
            $q->where('initiator_id', $user2)->where('partner_id', $user1);
        })->first();
    }


}
