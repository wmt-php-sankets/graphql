<?php

namespace App\Listener;

use App\Event\NotificatonLike;
use App\Event\NotificatonUser;
use App\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class like
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NotificatonUser $event
     * @return void
     */
    public function handle(NotificatonLike $event)
    {
        $user_id = Auth::id();
        $user_name = Auth::user()->name;
        $post = Notification::create([
            'title' => $user_name . " is Liked",
            'description' => "like description",
            'user_id' => $user_id,
            'create_user_id' => $event->post_details->user_id,
            'post_id' => $event->post_details->id,
            'notification_type' => "Like"
        ]);
    }
}
