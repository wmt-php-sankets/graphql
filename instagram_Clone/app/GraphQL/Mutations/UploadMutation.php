<?php

namespace App\GraphQL\Mutations;

use App\blocked_user;
use App\Comment;
use App\Event\NotificatonFollow;
use App\Event\NotificatonFollowdelete;
use App\Event\NotificatonLike;
use App\FollowUser;
use App\Post;
use App\Social_user;
use App\User;
use GraphQL\Type\Definition\ResolveInfo;
use http\Client\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Nuwave\Lighthouse\Console\IdeHelperCommand;
use Nuwave\Lighthouse\Exceptions\AuthenticationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Event\NotificatonUser;
use Laravel\Socialite\Facades\Socialite;
use PhpParser\Node\Stmt\DeclareDeclare;

class UploadMutation
{
    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        // TODO implement the resolver
    }


    #user ragistation
    public function register($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {

        $user = User::create(collect($args)->except('directive')->all());
        $token = auth()->login($user);

        return array_merge([
            'user' => $user,
            'meta' => ['message' => 'authorized', 'code' => 200, 'success' => true]], $this->respondWithToken($token));
    }

    private function respondWithToken($token)
    {
        return array(
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 160
        );
    }

    public function social_login($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {

        $user = Socialite::driver('facebook')->userFromToken($args['token']);

        $social_id = Social_user::where('social_id', $user->getId())->first();
        if ($social_id) {
            $token = auth()->login($social_id->user);
        } else {
            $user1 = User::create([
                'name' => $user->getName(),
                'username' => $user->getName(),
                'email' => $user->getEmail(),
                'about_me' => 'good',
            ]);
                $user2 = Social_user::create([
                'user_id' => $user1->id,
                'social_id' => $user->getId()
            ]);
            $token = auth()->login($user1);
        }
        return array_merge([
            'user' => User::find(auth()->payload()['sub'])
        ], $this->respondWithToken($token));
    }

    #user login
    public function login($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $credentials = ['username' => $args['username'], 'password' => $args['password']];
        if (!$token = auth()->attempt($credentials)) {
            return array('error' => 'Unauthorized', 401);
        }
        return array_merge([
            'user' => User::find(auth()->payload()['sub'])
        ], $this->respondWithToken($token));

    }

    //user logout
    public function logout()
    {
        auth()->logout();
        return ['meta' => ['message' => 'Successfully logged out', "success" => false, "code" => 200,]];
    }

    //user details
    public function me()
    {
        $user=Auth::user();
        return $user;
    }

    #searching
    public function search($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $search = $args['name'];
        $users = User::where('name', 'like', '%' . $search . '%')->get();
        return $users;
    }

    #follow/unfollow user
    public function followUnfollowUser($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {

        $auth_id = Auth::id();
        $follow_id = $args['follow_id'];
        $user = User::with('followedUnfollowUsers')->find($args['follow_id']);
        $user->followedUnfollowUsers()->toggle(Auth::id());
        if (sizeof($user->followedUnfollowUsers) == null) {
            event(new NotificatonFollow($user));
            return [
                    'user'=>$user->refresh(),
                'meta' => ['message' => 'you are follow', "success" => true, "code" => 200]];
        } else {
            event(new NotificatonFollowdelete($user));
            return [
                'user'=>$user->refresh(),
                'meta' => ['message' => 'you are unfollow', "success" => true, "code" => 200]];
        }

    }

    #useracceptrequest
    public function userFollowRequest($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $authid = Auth::id();
        $followuserid = $args['user_id'];

        $check = FollowUser::where('user_id', '=', $followuserid)
            ->where('Followed_user_id', '=', $authid)
            ->update(['status' => 1]);

        return [
            'user'=>$check->refresh(),
            'meta' => ['message' => 'Request accept', "success" => true, "code" => 200,]];

    }

    #userdeleterequest
    public function userUnFollow($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $authid = Auth::id();
        $followuserid = $args['user_id'];
        $check = FollowUser::where('user_id', '=', $followuserid)
            ->where('Followed_user_id', '=', $authid)
            ->delete();
        return [
            'user' => $check->refresh(),
            'meta' => ['message' => 'unfollow successfully', "success" => true, "code" => 200,]];
    }


    #blockuser
    public function blockUser($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $user = User::with('blockUnblock')->find($args['blocked_user_id']);
        $user->blockUnblock()->toggle(Auth::id());
        $user->touch();
        return [
            'user' => $user->refresh(),
            'meta' => ['message' => 'Successfully Perform', "success" => true, "code" => 200]];
    }
    public function userProfileUpdate($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $userQuery = User::find(Auth::id());
        $userQuery->update(collect($args)->except('directive')->all());
        return ['user' => $userQuery->refresh(), 'meta' => ['message' => 'Successfully Updated', "success" => true, "code" => 200,]];
    }
    public function userProfilepictureUpdate($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $userQuery = User::find(Auth::id());
        $post_type = ['jpg', 'jpeg', 'png', 'gif'];
        $image = $args['profile_pic'];
        $extension = $image->getClientOriginalExtension();
        $originalname = $image->getClientOriginalName();
        $path = in_array($extension, $post_type) ? $image->storeAs(Auth::id(), $originalname, 'public') : $image->storeAs('userprofile', $originalname, 'public');
        $userQuery->update(collect($args)->except('directive')->all());
        return ['user' => $userQuery->refresh(), 'meta' => ['message' => 'Successfully ProfilePicture Add ', "success" => true, "code" => 200,]];
    }
}
