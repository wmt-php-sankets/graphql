<?php

namespace Mpociot\Firebase\Tests\Fixtures;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

class TestCase extends Model
{

    use SyncsWithFirebase;
}
