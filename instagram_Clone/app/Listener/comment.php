<?php

namespace App\Listener;

use App\Event\NotificatonUser;
use App\Notification;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class comment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NotificatonUser $event
     * @return void
     */
    public function handle(NotificatonUser $event)
    {

        $user_id = Auth::id($event->post_data->post->user->id);
        $username=$event->post_data->user->name;
        $post = Notification::create([
            'title' =>" $username"." comment",
            'description' => "user description",
            'user_id' => $user_id,
            'create_user_id' => $event->post_data->post->user_id,
            'post_id' => $event->post_data->post_id,
            'notification_type' => "Commit"
        ]);
    }
}
