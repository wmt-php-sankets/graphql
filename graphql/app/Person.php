<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table=people;
    public function phone()
    {
        return $this->hasOne('App\Phone');
    }
}
